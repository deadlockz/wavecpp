hexdump -n 100 -C test.wav

552180 Bytes laut ls -la
 File Size: 552k      Bit Rate: 353k
  Encoding: Unsigned PCM  
  Channels: 1 @ 8-bit    
Samplerate: 44100Hz      
Replaygain: off         
  Duration: 00:00:12.52 

The "40 1F 00 00" bytes equate to an integer whose hexadecimal value
is 00001F40 (remember that the integers are stored in a WAVE file
in the little endian format). A value of 00001F40 in hexadecimal
equates to a decimal value of 8000.

Similarly, the "44 AC 00 00" bytes equate to an integer whose hexadecimal
value is 0000AC44. A value of 0000AC44 in hexadecimal equates to a
decimal value of 44100. 

Data Type	Size (Bytes)	Value
FOURCC		4				'RIFF'
DWORD		4				Total file size, not including the first 8 bytes
FOURCC		4				'WAVE'
FOURCC		4				'fmt '
DWORD		4				Size of the WAVEFORMATEX data that follows.
WAVEFORMATEX 	Varies		Audio format header.
FOURCC		4				'data'
DWORD		4				Size of the audio data.
BYTE[]		Varies			Audio data. (Little Endian)

00000000  52 49 46 46 ec 6c 08 00  57 41 56 45 66 6d 74 20  |RIFF.l..WAVEfmt |
00000010  10 00 00 00 01 00 01 00  44 ac 00 00 44 ac 00 00  |........D...D...|
00000020  01 00 08 00 64 61 74 61  c8 6c 08 00 80 80 7f 80  |....data.l......|
00000030  80 7f 80 7f 7f 80 7f 80  80 80 80 7f 80 80 7f 80  |................|
00000040  7f 7f 80 7f 80 7f 80 7f  7f 7f 80 7f 80 80 80 80  |................|
00000050  7f 80 7f 7f 7f 80 7f 80  80 80 7f 7f 7f 7f 80 7f  |................|
00000060  80 80 80 80                                       |....|
00000064

