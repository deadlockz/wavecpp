#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <sys/time.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "waveHeader.h"
#include "solve.h"
using namespace std;
using namespace cv;

#define FRAMESIZE 45

void printResult(vector<double> y) {
	cout<<"f(x) = "<<y[0]<<" "<<endl;
	for(unsigned i=1; i<y.size(); i++) {
		if(y[i] == 0) continue; // Ci = 0 faellt weg
		cout<<"       ";
		
		string foo = "sin";
		if(i <= FRAMESIZE/2) foo="cos";

		if(y[i] < 0) {
			cout<<"-"<<setw(12)<<(-1*y[i]);
		} else {
			cout<<"+"<<setw(12)<<y[i];
		}
		
		cout<<" *"<<foo<<"("<<i<<" *2pi* x/"<<FRAMESIZE<<") "<<endl;
	}
}

double funky(double x, unsigned j) {
	if(j==0) {
		return 1;
	} else if(j <= FRAMESIZE/2) {
		return sin(2.0 * j * M_PI * x / FRAMESIZE);
	} else {
		return cos(2.0 * j * M_PI * x / FRAMESIZE);
	}
}

void findMaxi(vector<double> &y, vector<unsigned> &sinIds, vector<unsigned> &cosIds, bool show=false){
	for(unsigned j=1; j<FRAMESIZE; j++) {
		// !! Willkuerliche Festlegung !!!!
		if(
			y[j]>-100 &&
			y[j]<100 &&
			(y[j]<-10 || y[j]>10)
		) {
			if(show) cout<<"c"<<j<<" = "<<y[j]<<endl;
			if(j <= FRAMESIZE/2)
				sinIds.push_back(j);
			else
				cosIds.push_back(j);
		}
	}
}



// -----------------------------------------------------------------
int main(int argc, char *argv[]) {
	if(argc<2){
		cout<<"usage (read only WAV 8bit Mono 44kHz):"<<endl;
		cout<<"  "<<argv[0]<<" <file>"<<endl;
		return 1;
	}
	string workfileName = argv[1];

	// zum zeit messen fuer die analyse
	double start=0;
	struct timeval tm;
	double duration;

	vector<unsigned char> data;
	unsigned posDat = readHeader(workfileName, data);

	unsigned size = data.size() - posDat;
	vector<unsigned char> outData = headerMaker(size);

	Mat resultImg(256, FRAMESIZE * 10, CV_8UC3);
	rectangle(resultImg, Point2f(0, 0), Point2f(256, FRAMESIZE * 10), Scalar(0,0,0), -1);
	Point2f p1, p2;

	// run trough data 
	for(unsigned frame = 0; frame < (size - FRAMESIZE); frame = frame + FRAMESIZE) {
		vector<double> y;
		vector< vector<double> > A;
		
		// run trough Frame / get Frame
		for(unsigned i = 0; i < FRAMESIZE; i++) {
			// normierung 0..255 -> -1..1 ???
			// y.push_back(0.0078125 * data[posDat+frame+i] -1);
			y.push_back(data[posDat+frame+i] -1);
		}

		// analyse Frame ---------------------------------------------
		for(double x=0; x<FRAMESIZE; x = x+1) {
			vector<double> dummy;
			for(unsigned j=0; j<FRAMESIZE; j++) {
				dummy.push_back(funky(x, j));
			}
			A.push_back(dummy);
		}

		solve(A, y, true);
		vector<unsigned> sinIds, cosIds;

		if(frame==0) {
			cout<<"Erster Frame mit "<<FRAMESIZE<<" Datenwerten schaut so aus:"<<endl;
			printResult(y);

			cout<<"Betrachte nur noch:"<<endl;
			findMaxi(y, sinIds, cosIds, true);
			
			for(float i = 0; i < FRAMESIZE; i++) {
				p1.x = i*10;
				p2.x = p1.x + 10;

				// sin kurven malen
				for(unsigned si = 0; si < sinIds.size(); si++) {
					Scalar curveColor(255, 255*si/sinIds.size(), 0);
					p2.y = y[0] + y[sinIds[si]] * sin(2.0 * sinIds[si] * M_PI * i/FRAMESIZE);
					p1.y = p2.y;
					//resultImg.at<Vec3b>(p2.y, p2.x) = curveColor;
					line(resultImg, p1, p2, curveColor);
				}
				// cos kurven malen
				for(unsigned ci = 0; ci < cosIds.size(); ci++) {
					Scalar curveColor(0, 255*ci/cosIds.size(), 255);
					p2.y = y[0] + y[cosIds[ci]] * cos(2.0 * cosIds[ci] * M_PI * i/FRAMESIZE);
					p1.y = p2.y;
					line(resultImg, p1, p2, curveColor);
				}
			}
			
			namedWindow((string) "WAV", WINDOW_NORMAL);
			imshow((string) "WAV", resultImg);
			imwrite((string) "filter." + workfileName + (string) ".png", resultImg);
			waitKey(0);

			// zeit messen fuer die analyse
			gettimeofday(&tm, NULL);
			start = (double) (tm.tv_sec) + ((double) (tm.tv_usec))/1.0e6;

		}else {
			
			// analyse fure frame in vector scheiben
			findMaxi(y, sinIds, cosIds);
			for(float i = 0; i < FRAMESIZE; i++) {
				unsigned char newval = y[0];
				
				for(unsigned si = 0; si < sinIds.size(); si++) {
					newval += y[sinIds[si]] * sin(2.0 * sinIds[si] * M_PI * i/FRAMESIZE);
				}
				for(unsigned ci = 0; ci < cosIds.size(); ci++) {
					newval += y[cosIds[ci]] * cos(2.0 * cosIds[ci] * M_PI * i/FRAMESIZE);
				}
				outData.push_back(newval);
			}
		}
	}

	// zum zeit messen fuer die analyse
	gettimeofday(&tm, NULL);
	duration = (double) (tm.tv_sec) + ((double) (tm.tv_usec))/1.0e6 - start;
	cout<<endl<<"Dauer "<<(duration*1000)<<" ms"<<endl;

	workfileName = "filter." + workfileName;
	ofstream outfile(workfileName.c_str(), ofstream::binary);
	outfile.write(
		(char *) (&outData[0]),
		outData.size() * sizeof(outData[0])
	);
	outfile.close();

	return 0;
}
