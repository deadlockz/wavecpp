#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
#include <ctime>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include "waveHeader.h"
using namespace std;
using namespace cv;

// 1,5 sec
#define LIMIT 66150
#define FRAMESIZE 45

Scalar sumColor(255,255,255);
Scalar sinColor(255,100,0);
Scalar cosColor(0,50,255);
Scalar noiseColor(0,255,0);

// -----------------------------------------------------------------
int main(int argc, char *argv[]) {
	srand(time(0));
	if(argc<5){
		cout<<"usage (make only WAV 8bit Mono 44kHz):"<<endl;
		cout<<"  "<<argv[0]<<" <file> <freq1> <freq2> <true|false>"<<endl;
		return 1;
	}
	string workfileName = argv[1];
	int hz1 = atoi(argv[2]);
	int hz2 = atoi(argv[3]);
	string rauschenStr = argv[4];
	bool noise = true;
	if(rauschenStr == "false") noise = false;

	Mat resultImg(256, FRAMESIZE * 10, CV_8UC3);
	rectangle(resultImg, Point2f(0, 0), Point2f(256, FRAMESIZE * 10), Scalar(0,0,0), -1);
	Point2f p1, p2;
	
	vector<unsigned char> outData = headerMaker(LIMIT);
	float value1, value2, value3 = 0;
	for(unsigned i=0; i<LIMIT; i++) {
		value1 = 1 + sin(i* 2.0 * M_PI * hz1/44100.0);
		value2 = 1 + cos(i* 2.0 * M_PI * hz2/44100.0);
		if(noise == true) {
			value3 = (rand()%10 +1)/20.0; // 0.05 .. 0.5
			outData.push_back( 254.0*(value1+value2+value3)/4.5 );
		} else {
			outData.push_back( 254.0*((value1+value2)/4.0) );
		}
		
		if(i<=FRAMESIZE) {
			p1.x = i*10;
			p2.x = p1.x + 10;
			p1.y = 127;
			
			// cos
			p2.y = 188 - value2*64;
			rectangle(resultImg, p1, p2, cosColor, -1);
			
			// sin
			p2.y = 188 - value1*64;
			rectangle(resultImg, p1, p2, sinColor, -1);

			// noise
			p2.y = 127 - value3*32;
			rectangle(resultImg, p1, p2, noiseColor, -1);

			// sum
			p2.y = 255.0 - 254.0*(value1+value2+value3)/4.5;
			rectangle(resultImg, p1, p2, sumColor);
		}
	}

	namedWindow((string) "WAV", WINDOW_NORMAL);
	imshow((string) "WAV", resultImg);
	imwrite(workfileName + (string) ".png", resultImg);
	waitKey(0);

	ofstream outfile(argv[1], ofstream::binary);
	outfile.write(
		(char *) (&outData[0]),
		outData.size() * sizeof(outData[0])
	);
	outfile.close();

	return 0;
}
