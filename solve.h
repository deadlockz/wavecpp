#ifndef SOLVE_H
#define SOLVE_H

#include <vector>
using namespace std;

double myAbs(double dummy) {
	if(dummy<0) dummy *=-1;
	return dummy;
}

/// stufenform mit rueckeinsetzen um ergebnis in b ab zu legen
void solve(
	vector< vector<double> > & A,
	vector<double> & b,
	bool withPivot
) {
	int maxId, i, k, j, n = b.size();

	/// stufenform berechnen (LR zerlegung) -----------------------
	vector< vector<double> > L(n, vector<double>(n));

	/// durchlaufe alle n zeilen. Laufindex: k
	for(k=0; k<n-1; k++) {

		/// pivot-suche ab zeile & spalte k
		if(withPivot == true) {
			maxId = k;
			for(int i = k; i< n; i++)
				if(myAbs(A[i][k]) > myAbs(A[maxId][k])) maxId = i;

			if(maxId != k) {
				swap(A[maxId], A[k]);
				swap(b[maxId], b[k]);
			}
		}

		/// durchlaufe alle zeilen unter k. laufindex: i
		for (i=k+1; i<n; i++) {
			/** Bilde neue Vorfaktoren fuer spalte k, um dort durch
			  * subtraktion nullen zu bekommen */
			L[i][k] = A[i][k] / A[k][k];
		}

		/// durchlaufe alle zeilen unter k. laufindex: i
		for (i=k+1; i<n; i++) {
			/// durchlauf alle restlichen spalten der zeile i: Laufindex j
			for (j=k; j<n; j++) {
				/// Anpassung der Zelle (i,j): "ziehe vielfaches von zeile k ab"
				A[i][j] = A[i][j] - L[i][k] * A[k][j];
			}
			/** zur Zeile i gehoert auch der inhomogene Teil der
			 * Gleichung (b-Vektor) der auch geaendert werden muss ! */
			b[i] = b[i] - L[i][k] * b[k];
		}
	}

	/// durchlaufe zum loesen von unten durch matrix -------------
	for(i = n-1; i>=0; i--) {
		double sum = 0;
		for(j = i+1; j<n; j++)
			sum = sum + A[i][j] * b[j];
		/// in b werden xn bis x1 gespeichert
		b[i] = (b[i] - sum) / A[i][i];
	}
}

#endif