#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>
//#include <ctime>

#include "waveHeader.h"
using namespace std;

// 1,5 sec
#define LIMIT 66150
#define FRAMESIZE 45

// -----------------------------------------------------------------
int main(int argc, char *argv[]) {
    srand(4711);
    if(argc<5){
        cout<<"usage (make only WAV 8bit Mono 44kHz):"<<endl;
        cout<<"  "<<argv[0]<<" <file> <freq1> <freq2> <true|false>"<<endl;
        return 1;
    }
    string workfileName = argv[1];
    int hz1 = atoi(argv[2]);
    int hz2 = atoi(argv[3]);
    string rauschenStr = argv[4];
    bool noise = true;
    if(rauschenStr == "false") noise = false;

    vector<unsigned char> outData = headerMaker(LIMIT);
    float value1, value2, value3 = 0;
    for(unsigned i=0; i<LIMIT; i++) {
        value1 = 1 + sin(i* 2.0 * M_PI * hz1/44100.0);
        value2 = 1 + cos(i* 2.0 * M_PI * hz2/44100.0);
        if(noise == true) {
            value3 = (rand()%10 +1)/20.0; // 0.05 .. 0.5
            outData.push_back( 254.0*(value1+value2+value3)/4.5 );
        } else {
            outData.push_back( 254.0*((value1+value2)/4.0) );
        }
    }

    ofstream outfile(argv[1], ofstream::binary);
    outfile.write(
        (char *) (&outData[0]),
        outData.size() * sizeof(outData[0])
    );
    outfile.close();

    return 0;
}
