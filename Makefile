CC=g++
CFLAGS=-march=native -pipe -O2 -Wall
LDFLAGS=-lm

CCFILES = $(wildcard ./*.cpp)
OFILES = $(CCFILES:.cpp=.o)
BIN = $(OFILES:.o=.exe)

all: waveFilter.exe waveMaker.exe

%.exe: %.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.exe *.o

