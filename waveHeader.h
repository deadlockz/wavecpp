#ifndef WAVEHEADER_H
#define WAVEHEADER_H

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

template <class T>
void endswap(T *objp){
	unsigned char *memp = reinterpret_cast<unsigned char*>(objp);
	reverse(memp, memp + sizeof(T));
}

// gives an unsigned int as a string with 8 chars size in little Endian style
string littleEndian4byte(unsigned const & value) {
	ostringstream oss;
	unsigned size = value;
	endswap(&size);
	oss<<hex<<size;
	string out = oss.str();
	unsigned leng = out.size();
	if(leng < 8) {
		for(unsigned i=leng; i<8; i++) {
			out = "0"+out;
		}
	}
	return out;
}

// make only WAV 8bit Mono 44kHz string-header for a given Data-Size(Byte)
string makeHeaderString(unsigned wavDataSize) {
	ostringstream oss;
	oss<<"52494646"; // "RIFF"
	oss<<littleEndian4byte(wavDataSize+36);
	oss<<"57415645666d7420"; // "WAVEfmt "
	oss<<littleEndian4byte(16);
	// WAVEFORMATEX
	oss<<"0100010044ac000044ac000001000800";
	oss<<"64617461"; // "data"
	oss<<littleEndian4byte(wavDataSize);
	string header = oss.str();
	return header;
}

// makes a sting like "52494646" to an unsigned char vector "RIFF"
vector<unsigned char> realHex(string seenHex) {
	vector<unsigned char> out;
	unsigned hnibble, lnibble;
	for(unsigned i=0; i<seenHex.size(); i = i+2) {
		stringstream ss1,ss2;
		ss1<<hex<<seenHex[i];
		ss2<<hex<<seenHex[i+1];
		ss1>>hnibble;
		ss2>>lnibble;
		hnibble*=16;
		out.push_back(hnibble+lnibble);
	}
	return out;
}

// makes only WAV 8bit Mono 44kHz header for a given Data-Size(Byte)
vector<unsigned char> headerMaker(unsigned wavDataSize) {
	return realHex(makeHeaderString(wavDataSize));
}

unsigned readHeader(string fileName, vector<unsigned char> & data) {
	ifstream f(fileName.c_str(), ifstream::binary);
	f.seekg(0, f.end);
	long fileSize = f.tellg();
	unsigned posWavFormSize = 0;

	data.resize(fileSize);
	cout<<"Datei "<<fileName<<" "<<fileSize<<" Byte"<<endl;

	f.seekg(0, f.beg);
	f.read(
		(char *) (&data[0]),
		data.size() * sizeof(data[0])
	);
	f.close();

	cout<<"Read the first 100 Bytes (size = 32 bit little endian): "<<endl;
	cout<<"'RIFF' (size-8) 'WAVE' 'fmt ' (size:format) (format)"<<endl;
	cout<<"'data' (size:data) (data .....)"<<endl;
	unsigned int chr;
	
	for(unsigned i=0; i<100; i++) {
		chr = data[i];
		if(chr<16) cout<<"0";
		cout<<hex<<chr<<" ";
		if((i+1)%4 == 0) cout<<" ";
		if((i+1)%16 == 0) cout<<endl;
		
		// find "WAVEfmt "
		if(
			i>8 &&
			data[i-7] == 'W' &&
			data[i-6] == 'A' &&
			data[i-5] == 'V' &&
			data[i-4] == 'E' &&
			data[i-3] == 'f' &&
			data[i-2] == 'm' &&
			data[i-1] == 't' &&
			data[i] == ' ' &&
			posWavFormSize == 0
		){
			posWavFormSize = i+1;
		}
	}
	
	cout<<"..."<<endl;
	cout<<"4Byte WAVEFORMATEX size is at Byte: "<<dec<<posWavFormSize<<endl;

	unsigned dataStart = posWavFormSize+4;
	unsigned WavFormSize = data[posWavFormSize];
	WavFormSize += data[posWavFormSize+1]*256;
	WavFormSize += data[posWavFormSize+2]*256*256;
	WavFormSize += data[posWavFormSize+3]*256*256*256;
	dataStart += WavFormSize +4 +4; //'data' (size:data)

	cout<<"WAVEFORMATEX size: "<<dec<<WavFormSize<<endl;
	cout<<"data starts at Byte: "<<dec<<dataStart<<endl;

	unsigned realSize = fileSize - dataStart;
	cout<<"real Data size: "<<dec<<realSize<<" Bytes"<<endl;

	return dataStart;
}

#endif